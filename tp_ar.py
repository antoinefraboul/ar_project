import numpy as np
import cv2
import xml.etree.ElementTree as ET
import csv
from obj_loader import *
import math

def hex_to_rgb(hex_color):
	hex_color = hex_color.lstrip('#')
	h_len = len(hex_color)
	return tuple(int(hex_color[i:i + h_len // 3], 16) for i in range(0, h_len, h_len // 3))

def projection_matrix(camera_parameters, homography):
	
	# Compute rotation along the x and y axis as well as the translation
	homography = homography * (-1)
	rot_and_transl = np.dot(np.linalg.inv(camera_parameters), homography)
	col_1 = rot_and_transl[:, 0]
	col_2 = rot_and_transl[:, 1]
	col_3 = rot_and_transl[:, 2]
	# normalise vectors
	l = math.sqrt(np.linalg.norm(col_1, 2) * np.linalg.norm(col_2, 2))
	rot_1 = col_1 / l
	rot_2 = col_2 / l
	translation = col_3 / l
	# compute the orthonormal basis
	c = rot_1 + rot_2
	p = np.cross(rot_1, rot_2)
	d = np.cross(c, p)
	rot_1 = np.dot(c / np.linalg.norm(c, 2) + d / np.linalg.norm(d, 2), 1 / math.sqrt(2))
	rot_2 = np.dot(c / np.linalg.norm(c, 2) - d / np.linalg.norm(d, 2), 1 / math.sqrt(2))
	rot_3 = np.cross(rot_1, rot_2)
	# finally, compute the 3D projection matrix from the model to the current frame
	projection = np.stack((rot_1, rot_2, rot_3, translation)).T
	
	return np.dot(camera_parameters, projection)

def render(img, obj, projection, model, color=False, scale=3):
	vertices = obj.vertices
	scale_matrix = np.eye(3) * scale
	h, w, _ = model.shape

	for face in obj.faces:
		face_vertices = face[0]
		points = np.array([vertices[vertex - 1] for vertex in face_vertices])
		points = np.dot(points, scale_matrix)
		# render model in the middle of the reference surface. To do so,
		# model points must be displaced
		points = np.array([[p[0] + w / 2, p[1] + h / 2, p[2]] for p in points])
		dst = cv2.perspectiveTransform(points.reshape(-1, 1, 3), projection)
		imgpts = np.int32(dst)
		if color is False:
			cv2.fillConvexPoly(img, imgpts, (204, 204, 51))
		else:
			color = hex_to_rgb(face[-1])
			color = color[::-1] # reverse
			cv2.fillConvexPoly(img, imgpts, color)
			
	return img

#src_video = "data/src_video_carte.mp4"
#ref_image = "data/ref_carte.jpg"
src_video = "data/src_video_servo.mp4"
ref_image = "data/ref_image_servo_crop.jpg"

obj = OBJ('data/steve/minecraft-steve.obj', swapyz=True)
scale = 60

MIN_MATCHES = 30

fname = "res/camera_data.csv"
file = open(fname, "wt")
writer = csv.writer(file)
#writer.writerow( ('num_image', 'tx', 'ty', 'tz', 'rx', 'ry', 'rz') )

ref = cv2.imread(ref_image)
ref = cv2.resize(ref, None, fx=0.2, fy=0.2)
orbRef = cv2.ORB_create()
keypoints_ref = orbRef.detect(ref, None)
keypoints_ref, descriptors_ref = orbRef.compute(ref, keypoints_ref)
#ref = cv2.drawKeypoints(ref, keypoints_ref, ref, color=(0,0,255), flags=0)
#cv2.imshow("Ref image", ref)

matcher = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)  

cap = cv2.VideoCapture(src_video)
if (cap.isOpened()== False): 
	print("Error opening video stream or file")

counter = 0
orb = cv2.ORB_create()
while(cap.isOpened()):
	ret, frame = cap.read()
	if ret == True:

		frame = cv2.resize(frame,None,fx=0.6,fy=0.6)

		# Detecting keypoints using ORB detector
		keypoints = orb.detect(frame, None)
		keypoints, descriptors = orb.compute(frame, keypoints)
		frame = cv2.drawKeypoints(frame, keypoints, frame, color=(0,255,0), flags=0)

		# Match keypoints
		matches = matcher.match(descriptors_ref, descriptors)
		# Sort matches
		matches = sorted(matches, key=lambda x: x.distance)

		src_pts = np.float32([keypoints_ref[m.queryIdx].pt for m in matches]).reshape(-1, 1, 2)
		dst_pts = np.float32([keypoints[m.trainIdx].pt for m in matches]).reshape(-1, 1, 2)
		
		# RANSAC is slightly better and have less jitter but PROSAC (RHO) is faster (real-time) and still have a good result.
		M, mask = cv2.findHomography(src_pts, dst_pts, method=cv2.RHO, ransacReprojThreshold=4.0,confidence=0.995)
		#M, mask = cv2.findHomography(src_pts, dst_pts, method=cv2.RANSAC, ransacReprojThreshold=4.0,maxIters=1000,confidence=0.995)

		if(M is not None):
			# Draw rectangle
			w, h, _ = ref.shape
			pts = np.float32([[0, 0], [0, w - 1], [h - 1, w - 1], [h - 1, 0]]).reshape(-1, 1, 2)
			# project corners into frame
			dst = cv2.perspectiveTransform(pts, M)  
			# connect them with lines
			frame = cv2.polylines(frame, [np.int32(dst)], True, 255, 3, cv2.LINE_AA) 

			# Solve PnP
			K = np.float64([[1.98636637e+03, 0.0, 9.64466820e+02],
							[0.0, 1.98738296e+03, 4.77496984e+02],
							[0.0, 0.0, 1.0]])            
			pts3d = np.float32([[0, 0, 0], [0, w - 1, 0], [h - 1, w - 1, 0], [h - 1, 0, 0]]).reshape(-1, 1, 3)
			
			# Transforming src_pts from 2D to 3D
			src_pts_3d = []
			for i in range(src_pts.shape[0]):
				point = src_pts[i]
				point3d = np.append(pts[0], [0.0])
				src_pts_3d.append(point3d)

			src_pts_3d_np = np.float32(src_pts_3d).reshape(-1, 1, 3)
			dist_coef = np.zeros(4)
			
			if (src_pts_3d_np.shape[0]>=6 and dst_pts.shape[0]>=6):
				ret, rvec, tvec = cv2.solvePnP(src_pts_3d_np, dst_pts, K, dist_coef)
				writer.writerow((counter, round(tvec[0][0], 4), round(tvec[1][0], 4), round(tvec[2][0], 4), round(rvec[0][0], 4), round(rvec[1][0], 4), round(rvec[2][0], 4)))
			else:
				writer.writerow((counter, None, None, None, None, None, None))

			# Display 3D model
			projection = projection_matrix(K, M)
			frame = render(frame, obj, projection, ref, False, scale)

			# Draw matches
			display_matches = cv2.drawMatches(ref, keypoints_ref, frame, keypoints, matches[:MIN_MATCHES], 0, flags=2)

			cv2.imshow('Frame',display_matches)
			if cv2.waitKey(3) & 0xFF == ord('q'):
				break

		else:
			writer.writerow((counter, None, None, None, None, None, None))
		counter += 1       
	else: 
		break
file.close()

file = open(fname, "rt")
reader = csv.DictReader(file)
#for row in reader:
	#print(row)
file.close()

cap.release()
cv2.destroyAllWindows()
